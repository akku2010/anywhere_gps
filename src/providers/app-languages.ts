import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Language } from './app.model';

@Injectable()
export class AppLanguages {

  private _lang: Language[] = [
    new Language(this.translate.instant('English'), 'en'),
    new Language(this.translate.instant('French'), 'french'),
  ];

  constructor(
    private translate: TranslateService
  ) { }

  get Languages() {
    return [...this._lang];
  }
}
